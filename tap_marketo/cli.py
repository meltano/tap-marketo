import json
import logging
import os
import sys
from os import environ as env
from datetime import datetime
from typing import Dict
from pathlib import Path

import click
import singer

from .client import MarketoClient
from .marketo_utils import MarketoUtils


# Set logging config
logger = singer.get_logger()
schema_dir = Path(os.path.dirname(__file__), "schemas")


@click.group(invoke_without_command=True)
@click.option("--config", "-c", required=True)
@click.pass_context
def cli(ctx, config):
    if not ctx.invoked_subcommand:
        ctx.invoke(extract)


@cli.command("extract")
@click.option("--log_only", default=False)
@click.pass_context
def extract(ctx, log_only: bool):
    """
    Handle creation of a MarketoClient instance and invoking its methods.

    config_file should be a path to a valid configuration file
    """
    config = ctx.parent.params["config"]

    with open(config, "r") as config_file:
        config_dict = json.load(config_file)

    marketo_client = MarketoClient(config_dict)
    for stream, records in marketo_client.get_all():
        if log_only:
            head, *_ = records
            logging.info(f"Columns for endpoint: {stream}")
            logging.info(head.keys())
            logging.info(f"Sample data for endpoint: {stream}")
            logging.info(head)
        else:
            singer.write_schema(
                stream_name=stream,
                schema=json.load(schema_dir.joinpath(f"{stream}.json").open()),
                key_properties=["id"],
            )
            singer.write_records(stream_name=stream, records=records)


@cli.command("create_keyfile")
@click.option("--minute_offset", default="70", type=int)
@click.option("--run_time", default=datetime.utcnow().isoformat())
@click.pass_context
def create_keyfile(ctx, minute_offset: str, run_time: str):
    """
    Create the keyfile from env vars.
    """
    config = ctx.parent.params["config"]

    marketo_utils = MarketoUtils(env.copy())
    marketo_utils.generate_keyfile(
        output_file=config, run_time=run_time, minute_offset=minute_offset
    )
