from setuptools import setup, find_packages

setup(
    name="meltano-tap-marketo",
    author="Meltano Team & Contributors",
    author_email="meltano@gitlab.com",
    description="Singer.io tap for extracting Marketo data.",
    url="https://gitlab.com/meltano/tap-marketo",
    version="0.2.0",
    packages=find_packages(exclude=["tests"]),
    entry_points={"console_scripts": ["tap-marketo = tap_marketo.cli:cli"]},
    include_package_data=True,
    install_requires=[
        "click>=6",
        "requests>=2.18",
        "singer-python",
        "pytz==2018.4",
    ],
)
